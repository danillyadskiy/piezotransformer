<?php

/**
 * Реализует функции обработки ответов
 */
class Response
{
    /**
     * Ответ в формате JSON
     *
     * @param mixed $content
     * @return bool
     */
    public function sendJsonContent($content)
    {
        echo json_encode($content);
        return true;
    }

    /**
     * Перенаправление по запрашиваемому маршруту
     *
     * @param string $path
     * @return bool
     */
    public function redirect($path)
    {
        $config = include(CONFIG_PATH);
        header("Location: ${$config->web['httpHostUrl']}/$path");
        return true;
    }

    /**
     * Ответ с кодом ошибки
     *
     * @param int $errorCode
     * @param string $errorMessage
     * @return bool
     */
    public function triggerHttpError($errorCode, $errorMessage)
    {
        header($errorMessage, TRUE, $errorCode);
        return true;
    }
}

//EOF