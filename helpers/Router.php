<?php

/**
 * Описывает маршрутизацию запросов
 *
 * @var mixed $routes
 */
class Router
{
    private $routes;

    /**
     * Получаем пользовательские маршруты
     *
     * @param mixed $routePath
     */
    function __construct($routePath)
    {
        $this->routes = include_once($routePath);
    }

    /**
     * Метод получает URI
     *
     * @return string
     */
    public static function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
        return '';
    }

    /**
     * Возвращает имя запрашиваемого контроллера
     *
     * @param string $uri
     * @return string
     */
    public static function getControllerName($uri)
    {
        // Разбиваем маршрут на сегменты.
        $segments = explode('/', $uri);

        // Получаем имя контроллера
        $controller = array_shift($segments);

        return $controller;
    }

    /**
     * Возвращает имя запрашиваемого действия
     *
     * @param string $uri
     * @return string
     */
    public static function getActionName($uri)
    {
        // Разбиваем маршрут на сегменты.
        $segments = explode('/', $uri);

        // Получаем имя действия
        $action = array_slice($segments, 1,1);

        // Преобразуем массив в строку
        $action = implode('',$action);
        return $action;
    }

    /**
     * Выполняет маршрутизацию запроса
     *
     * @return bool
     */
    public function run()
    {
        // Получаем URI
        $uri = $this->getURI();

        //Пытемся применить правило к запрошеному URL
        foreach ($this->routes as $pattern => $route) {
            //Если правило совпало - получаем внутренний маршрут
            if ($pattern === $uri) {
                $uri = $route;
                break;
            }
        }

        //Получаем имена контроллера и действия
        $controller = $this->getControllerName($uri);
        $action = $this->getActionName($uri);

        //Первая буква в названии контроллера заглавная
        $controller = ucfirst($controller).'Controller';
        $action = $action.'Action';

        // Подключаем файл контроллера, если он имеется
        $controllerFile = APP_PATH."/controllers/$controller.php";
        if (file_exists($controllerFile)) {
            include($controllerFile);
        }

        // Если не загружен нужный класс контроллера или в нём нет
        // нужного метода — 404
        if (!is_callable(array($controller, $action))) {
            include APP_PATH . "/controllers/ErrorController.php";
            call_user_func(array(new ErrorController, 'error404Action'));
        } else {
            // Иначе вызываем действие контроллера
            call_user_func(array(new $controller, $action));
        }

        return true;
    }
}

//EOF