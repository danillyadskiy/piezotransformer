<?php

/**
 * Реализует функционал по работе с куками
 */
class Cookies
{
    /**
     * Установить куку
     *
     * @param string $name
     * @param string $value
     * @return bool
     */
    public function set($name, $value)
    {
        //установка на 2 часа, httponly
        setcookie($name, $value, time()+7200, null, null, null, true);
        return true;
    }

    /**
     * Получить значение куки
     *
     * @param string $name
     * @return string|bool
     */
    public function get($name)
    {
        if (isset($_COOKIE[$name])) {
            return $_COOKIE[$name];
        } else {
            return false;
        }
    }

    /**
     * Уничтожить куку
     *
     * @param string $name
     * @return bool
     */
    public function destroy($name)
    {
        if (isset($_COOKIE[$name])) {
            //удаляем из памяти
            unset($_COOKIE[$name]);

            //время жизни -1 уничтожит куку у клиента
            setcookie($name, null, -1);
            return true;
        } else {
            return false;
        }
    }
}

//EOF