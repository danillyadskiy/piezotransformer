<?php

/**
 * Реализует функции обработки запросов
 */
class Request
{
    /**
     * Проверка на условие входящий запрос типа GET
     *
     * @return bool
     */
    public function isGet()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверка на условие входящий запрос типа POST
     *
     * @return bool
     */
    public function isPost()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверка на условие входящий запрос типа AJAX
     *
     * @return bool
     */
    public function isAjax()
    {
        if (
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Принимает имя параметра запроса GET
     * и возвращает его значение
     *
     * @param string $name
     * @return mixed
     */
    public function get($name)
    {
        return $_GET[$name];
    }

    /**
     * Принимает имя параметра запроса POST
     * и возвращает его значение
     *
     * @param string $name
     * @return mixed
     */
    public function getPost($name)
    {
        return $_POST[$name];
    }

    /**
     * Проверяет соответствует ли запрошенное действие
     * элементам массива
     *
     * @param array $actions
     * @return bool
     */
    public function isMatch($actions)
    {
        $uri = Router::getURI();
        $requiredAction = Router::getActionName($uri);
        foreach ($actions as $action) {
            if ($action === $requiredAction) {
                return true;
            }
        }
        return false;
    }
}

//EOF