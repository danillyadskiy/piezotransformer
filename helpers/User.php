<?php

/**
 * Реализует функционал по работе с пользователями
 *
 * @var object $cookies
 */
class User
{
    private $cookies;

    function __construct()
    {
        $this->cookies = new Cookies();
    }

    /**
     * Проверка авторизован ли пользователь
     *
     * @return bool
     */
    public function isAuth(){
        if ($this->cookies->get('AUTH') === 'TRUE') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверка введеных пользователем данных
     *
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function isCorrect($username, $password)
    {
        $config = include(CONFIG_PATH);
        if (
            $username === $config->admin['username'] &&
            $password === $config->admin['password']
        ) {
            //устанавливае куки (пользователь авторизован)
            $this->cookies->set('AUTH', 'TRUE');
            return true;
        } else {
            return false;
        }
    }

    /**
     * Удаление данных авторизации пользователя
     *
     * @return bool
     */
    public function logout()
    {
        $this->cookies->destroy('AUTH');
        return true;
    }
}

//EOF