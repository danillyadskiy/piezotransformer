<?php

/**
 * Реализует функции работы с шаблонами
 */
class View
{
    /**
     * Рендер представления
     *
     * @param string $renderTemplate
     * @param mixed $renderContent
     * @return bool;
     */
    public function render($renderTemplate, $renderContent = [])
    {
        //запрашиваем имя контроллера
        $renderTemplateResource = Router::getControllerName($renderTemplate);

        //Рендер главного шаблона
        include_once(APP_PATH . "/views/index.phtml");
        return true;
    }
}

//EOF