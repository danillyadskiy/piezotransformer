<?php

/**
 * Точка входа, глобальные настройки проекта и
 * подключение всех модулей
 *
 * @const APP_PATH
 * @const CONFIG_PATH
 */

define('APP_PATH', dirname(__DIR__) . '/piezotransformer');
define('CONFIG_PATH', APP_PATH . "/config/config.php");

//Библиотека DataTable
require_once APP_PATH . "/lib/DataTable/DataTables.php";

//Хелперы
require_once APP_PATH . "/helpers/Router.php";
require_once APP_PATH . "/helpers/View.php";
require_once APP_PATH . "/helpers/Request.php";
require_once APP_PATH . "/helpers/Response.php";
require_once APP_PATH . "/helpers/User.php";
require_once APP_PATH . "/helpers/Cookies.php";

//Стили
require_once APP_PATH . "/assets/AppAssets.php";

//Контроллеры
require_once APP_PATH . "/controllers/Controller.php";

//Модели
require_once APP_PATH . "/models/Model.php";

//Подключаем пользовательскую маршрутизацию URL
$routes = APP_PATH . "/config/routes.php";

//Маршрутизация запроса, точка входа
$router = new Router($routes);
$router->run();

//EOF