<?php

/**
 * Реализует вывод страниц об ошибках
 */
class ErrorController extends Controller
{
    /**
     * Запрос отсутствующей страницы
     * код ошибки 404
     *
     * @return bool
     */
    public function error404Action()
    {
        return $this->view->render('error/404');
    }
}

//EOF