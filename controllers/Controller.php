<?php

/**
 * Реализует общий функционал для всех контроллеров
 *
 * @var object $view
 * @var object $request
 * @var object $response
 * @var object $user
 * @var object $cookies
 */
class Controller
{
    protected $view;
    protected $request;
    protected $response;
    protected $user;
    protected $cookies;

    function __construct()
    {
        $this->view = new View();
        $this->request = new Request();
        $this->response = new Response();
        $this->user = new User();
        $this->cookies = new Cookies();
    }
}

//EOF