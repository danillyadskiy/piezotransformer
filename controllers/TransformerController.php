<?php

include APP_PATH . "/models/Piezotransformer.php";

/**
 * Реализует передачу основной информации на обработку
 * и её вывод в представление
 */
class TransformerController extends Controller
{
    /**
     * Запрос страницы поверочных расчетов
     *
     * @return bool
     */
    public function forwardAction()
    {
        $model = new Piezotransformer();
        $data = $model->getCols(['id', 'Material']);
        return $this->view->render('transformer/forward', $data);
    }

    /**
     * Запрос страницы проектировочных расчетов
     *
     * @return bool
     */
    public function backwardAction()
    {
        $model = new Piezotransformer();
        $data = $model->getCols(['id', 'Material', 'je11s']);
        return $this->view->render('transformer/backward', $data);
    }

    /**
     * AJAX-запрос на поверочный расчет в режиме максимальной мощности
     * Отправляет результат в формате JSON
     *
     * @return bool
     */
    public function methodForwardCapacityAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $model = new Piezotransformer();
                $data['a1'] = $this->request->getPost('a1');
                $data['b1'] = $this->request->getPost('b1');
                $data['L1'] = $this->request->getPost('L1');
                $data['type'] = $this->request->getPost('type');
                $data += $model->findByID($this->request->getPost('material-id'));
                $model->setProp($data);
                $result = $model->calcForwardCapacity();
                return $this->response->sendJsonContent($result);
            }
        }
        return $this->response->triggerHttpError(400, 'Ошибка запроса');
    }

    /**
     * AJAX-запрос на поверочный расчет в режиме максимального КПД
     * Отправляет результат в формате JSON
     *
     * @return bool
     */
    public function methodForwardEfficiencyAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $model = new Piezotransformer();
                $data['a1'] = $this->request->getPost('a1');
                $data['b1'] = $this->request->getPost('b1');
                $data['L1'] = $this->request->getPost('L1');
                $data['type'] = $this->request->getPost('type');
                $data += $model->findByID($this->request->getPost('material-id'));
                $model->setProp($data);
                $result = $model->calcForwardEfficiency();
                return $this->response->sendJsonContent($result);
            }
        }
        return $this->response->triggerHttpError(400, 'Ошибка запроса');
    }

    /**
     * AJAX-запрос на проектировочный расчет классическим методом
     * Отправляет результат в формате JSON
     *
     * @return bool
     */
    public function methodBackwardClassicAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $model = new Piezotransformer();
                $data['ku'] = $this->request->getPost('ku');
                $data['u_out'] = $this->request->getPost('u_out');
                $data['r_load'] = $this->request->getPost('r_load');
                $data['type'] = $this->request->getPost('type');
                $data += $model->findByID($this->request->getPost('material-id'));
                $model->setProp($data);
                $result['type'] = $data['type'];
                $result += $model->calcBackwardClassic();
                return $this->response->sendJsonContent($result);
            }
        }
        return $this->response->triggerHttpError(400, 'Ошибка запроса');
    }

    /**
     * AJAX-запрос на проектировочный расчет точным методом
     * Отправляет результат в формате JSON
     *
     * @return bool
     */
    public function methodBackwardAccurateAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $model = new Piezotransformer();
                $data['ku'] = $this->request->getPost('ku');
                $data['u_out'] = $this->request->getPost('u_out');
                $data['r_load'] = $this->request->getPost('r_load');
                $data['type'] = $this->request->getPost('type');
                $data += $model->findByID($this->request->getPost('material-id'));
                $model->setProp($data);
                $result['type'] = $data['type'];
                $result += $model->calcBackwardAccurate();
                return $this->response->sendJsonContent($result);
            }
        }
        return $this->response->triggerHttpError(400, 'Ошибка запроса');
    }
}

//EOF