<?php

include APP_PATH . "/models/Piezotransformer.php";

//Упрощенное обращение к классам библиотеки
use
    DataTables\Editor,
    DataTables\Editor\Field;

/**
 * Реализует вход на страницу администратора
 * и передачу информации и вывод на
 * страницы админки
 */
class AdminController extends Controller
{
    /**
     * Проверяем авторизован ли пользователь и
     * соответсвует ли запрошенное
     * действие перечню из списка
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->request->isMatch(['signIn', 'logout'])) {
            if (!$this->user->isAuth()) {
                $this->response->redirect('admin/signIn');
            }
        }
    }

    /**
     * Вывод главной страницы администратора
     * с таблицей
     *
     * @return bool|mixed
     * @throws Exception
     */
    public function tableAction()
    {
        if ($this->request->isAjax()) {
            return
                Editor::inst($GLOBALS['db'], 'piezotransformer')
                ->field(
                    Field::inst('id'),
                    Field::inst('Material'),
                    Field::inst('Company'),
                    Field::inst('Country'),
                    Field::inst('ro'),
                    Field::inst('Kp'),
                    Field::inst('Qm'),
                    Field::inst('Qs11e'),
                    Field::inst('Qs12e'),
                    Field::inst('Qs13e'),
                    Field::inst('Qs33e'),
                    Field::inst('Qs44e'),
                    Field::inst('Qd31'),
                    Field::inst('Qd33'),
                    Field::inst('Qd15'),
                    Field::inst('Qe11t'),
                    Field::inst('Qe33t'),
                    Field::inst('Y11e'),
                    Field::inst('Y33e'),
                    Field::inst('Y33d'),
                    Field::inst('c11e'),
                    Field::inst('jc11e'),
                    Field::inst('c12e'),
                    Field::inst('jc12e'),
                    Field::inst('c13e'),
                    Field::inst('jc13e'),
                    Field::inst('c33e'),
                    Field::inst('jc33e'),
                    Field::inst('c44e'),
                    Field::inst('jc44e'),
                    Field::inst('c11d'),
                    Field::inst('jc11d'),
                    Field::inst('c12d'),
                    Field::inst('jc12d'),
                    Field::inst('c13d'),
                    Field::inst('jc13d'),
                    Field::inst('c33d'),
                    Field::inst('jc33d'),
                    Field::inst('c44d'),
                    Field::inst('jc44d'),
                    Field::inst('s11e'),
                    Field::inst('js11e'),
                    Field::inst('s12e'),
                    Field::inst('js12e'),
                    Field::inst('s13e'),
                    Field::inst('js13e'),
                    Field::inst('s33e'),
                    Field::inst('js33e'),
                    Field::inst('s44e'),
                    Field::inst('js44e'),
                    Field::inst('s11d'),
                    Field::inst('js11d'),
                    Field::inst('s12d'),
                    Field::inst('js12d'),
                    Field::inst('s13d'),
                    Field::inst('js13d'),
                    Field::inst('s33d'),
                    Field::inst('js33d'),
                    Field::inst('s44d'),
                    Field::inst('js44d'),
                    Field::inst('e31'),
                    Field::inst('je31'),
                    Field::inst('e33'),
                    Field::inst('je33'),
                    Field::inst('e15'),
                    Field::inst('je15'),
                    Field::inst('d31'),
                    Field::inst('jd31'),
                    Field::inst('d33'),
                    Field::inst('jd33'),
                    Field::inst('d15'),
                    Field::inst('jd15'),
                    Field::inst('h31'),
                    Field::inst('jh31'),
                    Field::inst('h33'),
                    Field::inst('jh33'),
                    Field::inst('h15'),
                    Field::inst('jh15'),
                    Field::inst('g31'),
                    Field::inst('jg31'),
                    Field::inst('g33'),
                    Field::inst('jg33'),
                    Field::inst('g15'),
                    Field::inst('jg15'),
                    Field::inst('e11s'),
                    Field::inst('je11s'),
                    Field::inst('e11t'),
                    Field::inst('je11t'),
                    Field::inst('e33s'),
                    Field::inst('je33s'),
                    Field::inst('e33t'),
                    Field::inst('je33t')
                )
                ->process($_POST)
                ->json();
        }
        return $this->view->render('admin/table');
    }

    /**
     * Вывод страницы для ввода данных в таблицу
     *
     * @return bool
     */
    public function inputDataAction()
    {
        return $this->view->render('admin/input-data');
    }

    /**
     * Получает данные из формы и заносит в базу
     *
     * @return bool
     */
    public function insertMaterialAction()
    {
        if ($this->request->isPost()) {
            $model = new Piezotransformer();
            $data['Material'] = $this->request->getPost('Material');
            $data['Company'] = $this->request->getPost('Company');
            $data['Country'] = $this->request->getPost('Country');
            $data['ro'] = $this->request->getPost('ro');
            $data['Kp'] = $this->request->getPost('Kp');
            $data['Qm'] = $this->request->getPost('Qm');
            $data['Elastic'] = 'CE';
            $data['c11e'] = $this->request->getPost('c11e');
            $data['c12e'] = $this->request->getPost('c12e');
            $data['c13e'] = $this->request->getPost('c13e');
            $data['c33e'] = $this->request->getPost('c33e');
            $data['c44e'] = $this->request->getPost('c44e');
            $data['jc11e'] = $this->request->getPost('jc11e');
            $data['jc12e'] = $this->request->getPost('jc12e');
            $data['jc13e'] = $this->request->getPost('jc13e');
            $data['jc33e'] = $this->request->getPost('jc33e');
            $data['jc44e'] = $this->request->getPost('jc44e');
            $data['Piezoelectric'] = 'e';
            $data['e31'] = $this->request->getPost('e31');
            $data['e33'] = $this->request->getPost('e33');
            $data['e15'] = $this->request->getPost('e15');
            $data['je31'] = $this->request->getPost('je31');
            $data['je33'] = $this->request->getPost('je33');
            $data['je15'] = $this->request->getPost('je15');
            $data['Permittivities'] = 'es';
            $data['e11s'] = $this->request->getPost('e11s');
            $data['e33s'] = $this->request->getPost('e33s');
            $data['je11s'] = $this->request->getPost('je11s');
            $data['je33s'] = $this->request->getPost('je33s');
            if ($model->callProc('InsertMaterial',$data)) {
                return $this->response->redirect('admin/table');
            }
        }
        return $this->response->triggerHttpError(400, 'Ошибка запроса');
    }

    /**
     * Удаление выбранных материалов из базы
     *
     * @return bool
     */
    public function deleteMaterialAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $model = new Piezotransformer();
                $materialID = $this->request->getPost('materialID');
                $result = $model->deleteByID($materialID);
                return $this->response->sendJsonContent($result);
            }
        }
        return $this->response->triggerHttpError(400, 'Ошибка запроса');
    }

    /**
     * Переход в админку, если пользователь авторизован.
     * Иначе обратно на страницу авторизации,
     * если данные были введены неверно или
     * пользователь не авторизован
     *
     * @return bool
     */
    public function signInAction()
    {
        //Проверка на корректность введенных данных
        if ($this->request->isPost()) {
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            if ($this->user->isCorrect($username, $password)) {
                return $this->response->redirect('admin/table');
            } else {
                return $this->view->render('admin/sign-in', ['error' => 'true']);
            }
        }
        return $this->view->render('admin/sign-in');
    }

    /**
     * Выход из панели администратора
     *
     * @return bool
     */
    public function logoutAction()
    {
        $this->user->logout();
        return $this->response->redirect('admin/signIn');
    }
}

//EOF