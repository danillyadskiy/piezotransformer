<?php

/**
 * Реализация основного функционала по работе с данными
 *
 * @var object(mysqli) $dbConnect
 * @var string $calledClassName
 */
class Model
{
    protected $dbConnect;
    protected $calledClassName;

    /**
     * Устанавливает имя класса из которого был вызван метод
     * и устанавливает соединение с БД
     */
    function __construct()
    {
        $config = include(CONFIG_PATH);

        //Имя класса, из которого был вызван метод
        $this->calledClassName = lcfirst(get_called_class());

        //Соединение с базой данных
        $this->dbConnect = mysqli_connect($config->db['host'],
                                           $config->db['username'],
                                           $config->db['password'],
                                           $config->db['dbname']);
        if (!$this->dbConnect) {
            die("Connection to DB failed");
        }
    }

    /**
     * Заносит новые данные в таблицу
     * При успешном выполнении возвращает true
     *
     * @param $data mixed
     * @return bool|mysqli_result
     */
    public function insert($data)
    {
        //Создаем массив ключей
        $keys = array_keys($data);

        //Помещаем каждый элемент массива в обратные кавычки
        $keys = "`".implode("`, `", $keys)."`";

        //Помещаем каждый элемент массива в обычные кавычки
        $values = "'".implode("', '", $data)."'";
        $sql = "INSERT INTO $this->calledClassName ($keys) ";
        $sql .= "VALUES ($values)";
        $query = $this->mysqlQuery($sql);
        return $query;
    }

    /**
     * Удаляет строки из таблицы по id
     * При успешном выполнении возвращает true
     *
     * @param $id array
     * @return bool|mysqli_result
     */
    public function deleteByID($id)
    {
        //Преобразует массив в строку
        $id = implode(", ", $id);
        $sql = "DELETE FROM $this->calledClassName";
        $sql .= " WHERE id IN ($id);";
        $query = $this->mysqlQuery($sql);
        return $query;
    }

    /**
     * Вызывает процедуру с
     * параметрами
     *
     * @param $procName string
     * @param $data mixed
     * @return bool|mysqli_result
     */
    public function callProc($procName, $data)
    {
        //Помещаем каждый элемент массива в обычные кавычки
        $values = "'".implode("', '", $data)."'";
        $sql = "CALL $procName ($values) ";
        $query = $this->mysqlQuery($sql);
        return $query;
    }

    /**
     * Получает указанные столбцы из
     * таблицы
     *
     * @param $colNames array[string]
     * @return bool|mixed
     */
    public function getCols($colNames)
    {
        $colNamesStr = "`".implode("`, `", $colNames)."`";
        $sql = "SELECT $colNamesStr FROM $this->calledClassName";
        $query = $this->mysqlQuery($sql);
        $result = [];
        while($row = mysqli_fetch_array($query)) {
            foreach ($colNames as $colName) {
                $result[$colName][] = $row[$colName];
            }
        }
        return $result;
    }

    /**
     * Получает запись из таблицы
     * по id
     *
     * @param $id int
     * @return bool|mixed
     */
    public function findByID($id)
    {
        $sql = "SELECT * FROM $this->calledClassName ";
        $sql .= "WHERE `id` = $id LIMIT 1";
        $query = $this->mysqlQuery($sql);
        $result = mysqli_fetch_array($query);
        return $result;
    }

    /**
     * Автоматически устанавливает св-ва объекта
     *
     * @param $object mixed
     * @return bool
     */
    public function setProp($object)
    {
        foreach (array_keys($object) as $key) {
            $this->$key = $object[$key];
        }
        return true;
    }

    /**
     * Посылает запрос в базу данных и возвращает ответ
     *
     * @param $sql string
     * @return bool|mysqli_result
     */
    private function mysqlQuery($sql)
    {
        return mysqli_query($this->dbConnect, $sql);
    }
}

//EOF