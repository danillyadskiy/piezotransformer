<?php

/**
 * Отвечает за работу с базой данных в панели администратора
 */
class Piezotransformer extends Model
{
    public $id;
    public $Material;
    public $Company;
    public $Country;
    public $ro;
    public $Kp;
    public $Qm;
    public $Qs11e;
    public $Qs12e;
    public $Qs13e;
    public $Qs33e;
    public $Qs44e;
    public $Qd31;
    public $Qd33;
    public $Qd15;
    public $Qe11t;
    public $Qe33t;
    public $Y11e;
    public $Y33e;
    public $Y33d;
    public $c11e;
    public $c12e;
    public $c13e;
    public $c33e;
    public $c44e;
    public $jc11e;
    public $jc12e;
    public $jc13e;
    public $jc33e;
    public $jc44e;
    public $c11d;
    public $c12d;
    public $c13d;
    public $c33d;
    public $c44d;
    public $jc11d;
    public $jc12d;
    public $jc13d;
    public $jc33d;
    public $jc44d;
    public $s11e;
    public $s12e;
    public $s13e;
    public $s33e;
    public $s44e;
    public $js11e;
    public $js12e;
    public $js13e;
    public $js33e;
    public $js44e;
    public $s11d;
    public $s12d;
    public $s13d;
    public $s33d;
    public $s44d;
    public $js11d;
    public $js12d;
    public $js13d;
    public $js33d;
    public $js44d;
    public $e31;
    public $e33;
    public $e15;
    public $je31;
    public $je33;
    public $je15;
    public $d31;
    public $d33;
    public $d15;
    public $jd31;
    public $jd33;
    public $jd15;
    public $h31;
    public $h33;
    public $h15;
    public $jh31;
    public $jh33;
    public $jh15;
    public $g31;
    public $g33;
    public $g15;
    public $jg31;
    public $jg33;
    public $jg15;
    public $e11s;
    public $e33s;
    public $je11s;
    public $je33s;
    public $e11t;
    public $e33t;
    public $je11t;
    public $je33t;

    public $a1;
    public $b1;
    public $L1;
    public $ku;
    public $u_out;
    public $r_load;
    public $type;

    /**
     * Подключает необходимый класс из библиотеки
     * и производит поверочный расчет
     * в режиме максимальной мощности
     *
     * @return mixed
     */
    public function calcForwardCapacity()
    {
        include_once(APP_PATH . "/models/lib/forward/capacity/$this->type.php");
        $transformer = new $this->type();
        return $transformer->calc($this->a1, $this->b1,$this->L1, $this->ro,
                                  $this->Qm, $this->Y33e, $this->Y11e, $this->Y33d,
                                  $this->d31, $this->d33, $this->g33, $this->e33t);
    }

    /**
     * Подключает необходимый класс из библиотеки
     * и производит поверочный расчет
     * в режиме максимальной мощности
     *
     * @return mixed
     */
    public function calcForwardEfficiency()
    {
        include_once(APP_PATH . "/models/lib/forward/efficiency/$this->type.php");
        $transformer = new $this->type();
        return $transformer->calc($this->a1, $this->b1,$this->L1, $this->ro,
                                  $this->Qm, $this->Y33e, $this->Y11e, $this->Y33d,
                                  $this->d31, $this->d33, $this->g33, $this->e33t);
    }

    /**
     * Подключает необходимый класс из библиотеки
     * и производит проектировочный расчет
     * классическим методом
     *
     * @return mixed
     */
    public function calcBackwardClassic()
    {
        include_once(APP_PATH . "/models/lib/backward/classic/$this->type.php");
        $transformer = new $this->type();
        return $transformer->calc($this->ku, $this->u_out, $this->r_load, $this->ro,
                                  $this->Qm, $this->Y33e, $this->Y11e, $this->Y33d,
                                  $this->d31, $this->d33, $this->g33, $this->e33t);
    }

    /**
     * Подключает необходимый класс из библиотеки
     * и производит проектировочный расчет
     * точным методом
     *
     * @return mixed
     */
    public function calcBackwardAccurate()
    {
        include_once(APP_PATH . "/models/lib/backward/accurate/$this->type.php");
        $transformer = new $this->type();
        return $transformer->calc($this->ku, $this->u_out, $this->r_load, $this->ro,
                                  $this->Qm, $this->Qs11e, $this->Qs33e, $this->Qd33, $this->Qe33t, $this->Y33e,
                                  $this->Y11e, $this->Y33d, $this->d31, $this->d33,
                                  $this->g33, $this->e33t);
    }
}

//EOF