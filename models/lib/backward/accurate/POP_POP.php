<?php

/**
 * Проектировочный расчет точным методом
 * поперечно-поперечного трансформатора
 */
class POP_POP
{
    public function calc($ku, $u_out, $r_load, $ro,
                         $Qm,  $Qs11e, $Qs33e, $Qd33, $Qe33t, $Y33e,
                         $Y11e, $Y33d, $d31, $d33,
                         $g33, $e33t)
    {
        $L1 = $u_out/20000;
        $k33 = sqrt($g33*$d33*$Y33e);
        $k31 = sqrt($d31**2*$Y33e/$e33t);
        $ku0 = 2*$ku;
        $Qx = ($Qs11e*$Qe33t+1)/($Qs11e*(1-$k33**2)-$k33**2*$Qe33t);
        $Qfi = ($Qd33*$Qs11e+1)/($Qd33-$Qs11e);
        $Qxi = ($Qd33-$Qs33e)/($Qd33*$Qs33e+1);
        $k_n_fi = (1+1/($Qs11e+$Qd33))/1+1/$Qs11e**2;
        $k_n_xi = (1+1/($Qs33e+$Qd33))/1+1/$Qs33e**2;
        $k_z0 = $d33*$Y33e/(1+1/$Qs11e**2)*(1+1/($Qs11e*$Qd33));
        $I_u0_er = sqrt((1-1/$Qxi**2)*(1-1/$Qx**2-4*$Qxi/($Qx*($Qxi**-1))));
        $I_u0_et = sqrt((1-1/$Qfi**2)*(1-1/$Qx**2-4*$Qfi/($Qx*($Qfi**-1))));
        $I_n_fi = sqrt(1+1/$Qfi**2);
        $I_n_xi = sqrt(1+1/$Qxi**2);
        $I_r2_er = (1-1/$Qxi**2)*((($Qx**2-1)*($Qxi**2-1)-4*$Qxi)/($Qx**2*($Qxi**2-1)));
        $I_r2_et = (1-1/$Qxi**2)*((($Qx**2-1)*($Qxi**2-1)-4*$Qxi)/($Qx**2*($Qxi**2-1)));
        $a1 = abs(4*$Qm*$Y33e*$g33*$d31*$L1/(M_PI**2*(1-$k33**2)*$ku0))*$k_n_fi*$k_n_fi*$I_u0_et*$I_n_fi/$k_z0;
        $ce = sqrt($Y33e/$ro);
        $b1 = abs($Qm*($k33**2)*($L1**2)/(M_PI**3*$ce*(1-$k33**2)**2*$e33t*$r_load*$a1))*$k_n_fi*$I_r2_et;
        $a2 = $a1;
        $b2 = abs($a1*$b1*sqrt(1-$k33**2)/$a2);
        $L2 = $L1/sqrt(1-$k33**2);
        $f = $ce/4*$L1;
        $result['a1'] = $a1;
        $result['a2'] = $a2;
        $result['b1'] = $b1;
        $result['b2'] = $b2;
        $result['L1'] = $L1;
        $result['L2'] = $L2;
        $result['f'] = $f;
        return $result;
    }
}