<?php

/**
 * Проектировочный расчет классическим методом
 * поперечно-продольного трансформатора
 */
class POP_PROD
{
    public function calc($ku, $u_out, $r_load, $ro,
                  $Qm, $Y33e, $Y11e, $Y11d, $d31, $d33, $g33,
                  $e33t)
    {
        $L1 = $u_out/20000;
        $k33 = sqrt($g33*$d33*$Y33e);
        $k31 = sqrt($d31**2*$Y33e/$e33t);
        $ku0 = 2*$ku;
        $a1 = abs(4*$Qm*$Y33e*$g33*$d31*$L1/(M_PI**2*(1-$k33**2)*$ku0));
        $ce = sqrt($Y33e/$ro);
        $b1 = abs($Qm*($k33**2)*($L1**2)/(M_PI**3*$ce*(1-$k33**2)**2*$e33t*$r_load*$a1));
        $a2 = $a1;
        $b2 = $a1*$b1*sqrt(1-$k33**2)/$a2;
        $L2 = $L1/sqrt(1-$k33**2);
        $f = $ce/4*$L1;
        $result['a1'] = $a1;
        $result['a2'] = $a2;
        $result['b1'] = $b1;
        $result['b2'] = $b2;
        $result['L1'] = $L1;
        $result['L2'] = $L2;
        $result['f'] = $f;
        //$w = 2*M_PI*$f;
        //$Xet = $a1/($w*$e33t*(1-$k31**2)*$b1*$L1);
        //$Re = M_PI*$a1*($ce*$Qm*$Y33e*$d31**2*$b1);
        //$Le = $Xet/($w*(1 + ($Xet/$Re)**2));
        return $result;
    }
}