<?php

/**
 * Проектировочный расчет классическим методом
 * поперечно-поперечного трансформатора
 */
class POP_POP
{
    public function calc($ku, $u_out, $r_load, $ro,
                         $Qm, $Y33e, $Y11e, $Y33d,
                         $d31, $d33, $g33, $e33t)
    {
        $L1 = $u_out/20000;
        $k33 = sqrt($g33*$d33*$Y33e);
        $k31 = sqrt($d31**2*$Y33e/$e33t);
        $ku0 = 2*$ku;
        $a1 = 0.001;
        $cd = sqrt($Y33d/$ro);
        $b1 = abs(2*$d31*$Y33d**2*$Qm*$a1/($Y33d*M_PI**3*$cd*$e33t*(1-$k31**2)**2*$r_load));
        $a2 = $a1;
        $b2 = $a1*$b1*sqrt(1-$k33**2)/$a2;
        $L2 = $L1/sqrt(1-$k33**2);
        $f = $cd/4*$L1;
        $result['a1'] = $a1;
        $result['a2'] = $a2;
        $result['b1'] = $b1;
        $result['b2'] = $b2;
        $result['L1'] = $L1;
        $result['L2'] = $L2;
        $result['f'] = $f;
        //$w = 2*M_PI*$f;
        //$Xet = $a1/($w*$e33t*(1-$k31**2)*$b1*$L1);
        //$Re = M_PI*$a1*($ce*$Qm*$Y33e*$d31**2*$b1);
        //$Le = $Xet/($w*(1 + ($Xet/$Re)**2));
        return $result;
    }
}