<?php

/**
 * Поверочный расчет в режиме макс. КПД
 * поперечно-продольного трансформатора
 */
class POP_PROD
{
    public function calc($a1, $b1, $L1, $ro,
                         $Qm, $Y33e, $Y11e, $Y33d,
                         $d31, $d33, $g33, $e33t)
    {
        $d33 = abs($d33);
        $k33 = sqrt($g33*$d33*$Y33e);
        $n_xi = $k33**2*(1-$k33**2)*$a1*$b1/($g33*$L1);
        $ce = sqrt($Y33e/$ro);
        $w = M_PI*$ce/(2*$L1);
        $Xer = $L1/($w*$e33t*(1-$k33**2)**2*$a1*$b1);
        $Rn = range(10000, 10000000, 50000);
        $n = [];
        foreach ($Rn as $value) {
            $r = 2*$n_xi**2*$value*$Xer**2/($value**2+$Xer**2);
            $n[] = $n_xi**2*$Xer/($r+$n_xi**2*$Xer)*100;
        }
        $result['Rn'] = $Rn;
        $result['n'] = $n;
        return $result;
    }
}