<?php

/**
 * Поверочный расчет в режиме макс. КПД
 * поперечно-поперечный трансформатора
 */
class POP_POP
{
    public function calc($a1, $b1, $L1, $ro,
                         $Qm, $Y33e, $Y11e, $Y33d,
                         $d31, $d33, $g33, $e33t)
    {
        $d31 = abs($d31);
        $k31 = sqrt($d31**2*$Y33e/$e33t);
        $n_fi = $b1*$d31*$Y33e;
        $ce = sqrt($Y33d/$ro);
        $w = M_PI*$ce/(2*$L1);
        $Xet = $a1/($w*$e33t*(1-$k31**2)*$L1*$b1);
        $Rn = range(10000, 10000000, 50000);
        $n = [];
        foreach ($Rn as $value) {
            $r = 2*$n_fi**2*$value*$Xet**2/($value**2+$Xet**2);
            $n[] = $n_fi**2*$Xet/($r+$n_fi**2*$Xet)*100;
        }
        $result['Rn'] = $Rn;
        $result['n'] = $n;
        return $result;
    }
}