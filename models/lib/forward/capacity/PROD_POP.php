<?php

/**
 * Поверочный расчет в режиме макс. мощности
 * продольно-поперечного трансформатора
 */
class PROD_POP
{
    public function calc($a1, $b1, $L1, $ro,
                         $Qm, $Y33e, $Y11e, $Y33d,
                         $d31, $d33, $g33, $e33t)
    {
        $d31 = abs($d31);
        $d33 = abs($d33);
        $k33 = sqrt($g33*$d33*$Y33e);
        $k31 = sqrt($d31**2*$Y33e/$e33t);
        $n_xi = $k33**2*(1-$k33**2)*$a1*$b1/($g33*$L1);
        $n_fi = $b1*$d31*$Y33e;
        $ce = sqrt($Y11e/$ro);
        $w = M_PI*$ce/(2*$L1);
        $Xet = $a1/($w*$e33t*(1-$k31**2)*$L1*$b1);
        $Rn = range(10000, 10000000, 50000);
        $ku = [];
        foreach ($Rn as $value) {
            $r2 = 4*$n_fi**2*$value*$Xet**2/($value**2+$Xet**2);
            $ku[] = 2*$n_xi*sqrt($value*$r2)/(2*$r2);
        }
        $result['Rn'] = $Rn;
        $result['ku'] = $ku;
        return $result;
    }
}