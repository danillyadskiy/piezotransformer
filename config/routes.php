<?php

/**
 * Файл описывает маршруты, задаваемые
 * пользователем вручную
 *
 * @return mixed
 */

return array(
    //Запрос главной страницы
    //(это страница прямого метода расчетов)
    'index' => 'transformer/forward',
    '' => 'transformer/forward',

    //Запрос страницы администратора
    'admin' => 'admin/table',
);

//EOF