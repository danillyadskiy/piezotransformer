/**
 * Функция построения графика
 */
function widget_linechart(value1, value2, type){

    let data = [];
    for (let i=0;i<value1.length;i++) {
        data[i] = [value1[i]/1000000, value2[i]];
    }

    let yaxis = '';
    if (type === 'capacity') {
        yaxis = 'Коэф. усиления по напряжению';
    } else if (type === 'efficiency') {
        yaxis = 'КПД, %';
    }

    let color1 = tinycolor( App.color.primary ).lighten( 5 ).toString();

    let plot_statistics = $.plot($("#line-chart"), [{
        data: data,
    }
    ], {
        series: {
            lines: {
                show: true,
                lineWidth: 2,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.1
                    }, {
                        opacity: 0.1
                    }
                    ]
                }
            },
            points: {
                show: true
            },
            shadowSize: 0
        },
        legend:{
            show: false
        },
        grid: {
            margin: {
                left: 23,
                right: 30,
                top: 20,
                botttom: 40
            },
            labelMargin: 15,
            axisMargin: 500,
            hoverable: true,
            clickable: true,
            tickColor: "rgba(0,0,0,0.15)",
            borderWidth: 0
        },
        tooltip:{
            show: true,
            cssClass: "tooltip-chart",
            content: "<div class='content-chart'> <span> %s </span> <div class='label'> <div class='label-x'> %x.0 " +
                "</div> - <div class='label-y'> %y.0 </div> </div></div>",
            defaultTheme: false
        },
        colors: [color1],
        xaxis: {
            position: 'bottom',
            axisLabel: 'Сопротивление нагрузки, МОм',
        },
        yaxis: {
            position: 'left',
            axisLabel: yaxis,
            axisLabelPadding: 3
        }
    });

    widget_tooltipPosition('line-chart', 40);
}

/**
 * Функция позиционирования курсора
 * на графике
 *
 * @param string id
 * @param number top
 */
function widget_tooltipPosition(id, top){
    $('#'+id).bind("plothover", function (event, pos, item) {
        let widthToolTip = $('.tooltip-chart').width();
        if(item){
            $(".tooltip-chart")
                .css({top: item.pageY - top, left: item.pageX - (widthToolTip / 2)})
                .fadeIn(200);
        }else{
            $(".tooltip-chart").hide();
        }
    });
}