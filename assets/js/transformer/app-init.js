/**
 * Файл настроек подключаемых модулей
 */

$(document).ready(function(){

    /**
     * Инициализируем JS
     */
    App.init();

    /**
     * Выпадающие списки
     */
    App.formElements();
    $(".transformer-type").find(".select2-selection__placeholder").html('Тип трансформатора');
    $(".material-type").find(".select2-selection__placeholder").html('Тип материала');

    //Меняем местами select и создаваемый им контейнер
    $( "form" ).each(function() {
        $(this).find('.select2-container').each(function () {
            $(this).after($(this).parent().find('select'));
        })
    });

    /**
     * Валидация
     */
    $('form').parsley();
    $( "select" ).each(function() {
        $(this).parsley().options.requiredMessage = "Выберите тип.";
    });

    $("select").change(function() {
        $(this).parsley().validate();
    });

    /*Замена запятой на точку*/
    $("input[data-parsley-type='number']").on( "keyup", function() {
        this.value = this.value.replace(/,/g,".");
    });

    /*Убираем нули перед целым числом
      по нажатию на форму отправки*/
    $('button[type="submit"]').click(function() {
        //Меняем только импуты родительской формы
        const pform = this.closest("form");

        $(pform).find("input[data-parsley-type='number']").each(function() {

            if ($(this).parsley().isValid()) {
                this.value = this.value.replace(this.value, Number(this.value));
            }

        });
    });
});