/**
 * AJAX код для отправки данных на
 * проектировочный расчет классическим методом
 * и возврат результатов в блок results
 */
$(document).ready(function() {

    $('#form-method-backward-classic').submit(function(e) {
        e.preventDefault();
        $.ajax({

            beforeSend: function () {
                //Начинается работа прелоадера
                $('.be-loading').addClass('be-loading-active');
            },

            complete: function () {
                //Заканчивается работа прелоадера
                $('.be-loading').removeClass('be-loading-active');
            },

            type: 'POST',
            url: '/transformer/methodBackwardClassic',
            data: $(this).serialize(),

        }).done(function (response) {
            const jsonData = JSON.parse(response);

            //Удаляем предыдущий вывод результатов
            clearPreviousResults();

            //Помещаем посчитаные размеры трансформатора
            // в блок результатов
            putBackwardResults(jsonData);

        }).fail(function () {
            //Сообщение об ошибке
            alert('Что-то пошло не так!');
        });
    });

    $('#form-method-backward-accurate').submit(function(e) {
        e.preventDefault();
        $.ajax({

            beforeSend: function () {
                //Начинается работа прелоадера
                $('.be-loading').addClass('be-loading-active');
            },

            complete: function () {
                //Заканчивается работа прелоадера
                $('.be-loading').removeClass('be-loading-active');
            },

            type: 'POST',
            url: '/transformer/methodBackwardAccurate',
            data: $(this).serialize(),

        }).done(function (response) {
            const jsonData = JSON.parse(response);

            //Удаляем предыдущий вывод результатов
            clearPreviousResults();

            //Помещаем посчитаные размеры трансформатора
            // в блок результатов
            putBackwardResults(jsonData);

        }).fail(function () {
            //Сообщение об ошибке
            alert('Что-то пошло не так!');
        });
    });

});

