/**
 * Общие функции главного шаблона
 */

/**
 * Удаляет предыдущий вывод результатов
 */
function clearPreviousResults() {
    $('#results>.card-body').empty();
}

/**
 * Формирует HTML-код и помещает
 * в блок вывода результатов
 * (для поверочного расчета!)
 *
 * @param mixed results
 * @returns {boolean}
 */
function putForwardCapacityResults(results) {
    $('#results>.card-body').append(
        `<div class="widget">`+
            `<div class="widget-head">`+
                `<span class="title">График зависимости</span>`+
                `<span class="description">На графике показано расчетное усиление от сопротивления нагрузки</span>`+
            `</div>`+
            `<div class="widget-chart-container">`+
                `<div id="line-chart">`+
                `</div>`+
            `</div>`+
        `</div>`
    );
    widget_linechart(results['Rn'], results['ku'], 'capacity');
    return true;
}

/**
 * Формирует HTML-код и помещает
 * в блок вывода результатов
 * (для поверочного расчета!)
 *
 * @param mixed results
 * @returns {boolean}
 */
function putForwardEfficiencyResults(results) {
    $('#results>.card-body').append(
        `<div class="widget">`+
            `<div class="widget-head">`+
                `<span class="title">График зависимости</span>`+
                `<span class="description">На графике показана зависимость КПД от сопротивления нагрузки</span>`+
            `</div>`+
            `<div class="widget-chart-container">`+
                `<div id="line-chart">`+
                `</div>`+
            `</div>`+
        `</div>`
    );
    widget_linechart(results['Rn'], results['n'], 'efficiency');
    return true;
}

/**
 * Формирует HTML-код и помещает
 * в блок вывода результатов
 * (для проектировочного расчета!)
 *
 * @param mixed results
 * @returns {boolean}
 */
function putBackwardResults(results) {
    $('#results>.card-body').append(
        `<img class="card-img" 
            alt="${results['type']}" 
            src="/assets/img/transformer-types/${results['type']}.png">`+
        `<span class="result-size text-left"">Толщина 1-й секции a = ${results['a1'].toFixed(5)} м</span>`+
        `<span class="result-size text-left"">Толщина 2-й секции a' = ${results['a2'].toFixed(5)} м</span>`+
        `<span class="result-size text-left"">Ширина 1-й секции b = ${results['b1'].toFixed(5)} м</span>`+
        `<span class="result-size text-left"">Ширина 2-й секции b' = ${results['b2'].toFixed(5)} м</span>`+
        `<span class="result-size text-left"">Длинна 1-й секции L = ${results['L1'].toFixed(5)} м</span>`+
        `<span class="result-size text-left"">Длинна 2-й секции L' = ${results['L2'].toFixed(5)} м</span>`+
        `<span class="result-size text-left"">Рабочая частота f = ${results['f'].toFixed(5)} Гц</span>`
    );
    return true;
}