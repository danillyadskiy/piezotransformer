/**
 * Настройки и генерация аттрибутов окон ввода
 */

$(document).ready(function(){

    /**
     *
     * @type {{
     *      min: number, max: number,
     *      label: string, ph: string, dimension: string
     *      }}
     */
    const inputConfig = {
        "a1":{
            "min": 0.00000000001,
            "label": "a",
            "ph": "Толщина",
            "dimension": "м"
        },
        "b1":{
            "min": 0.00000000001,
            "label": "b",
            "ph": "Ширина",
            "dimension": "м"
        },
        "L1":{
            "min": 0.00000000001,
            "label": "L",
            "ph": "Длинна",
            "dimension": "м"
        },
        "ku":{
            "min": 0.00000000001,
            "label": "k<sub>u</sub>",
            "ph": "Коэффициент усиления",
            "dimension": ""
        },
        "u_out":{
            "min": 0.00000000001,
            "label": "U<sub>вых</sub>",
            "ph": "Выходное напряжение",
            "dimension": "В"
        },
        "r_load":{
            "min": 0.00000000001,
            "label": "R<sub>н</sub>",
            "ph": "Сопротивление нагрузки",
            "dimension": "Ом"
        },
    };

    /**
     * Генерирует окно ввода по имени
     */
    $("input").each(function() {
        const inputName = $(this).attr('name');
        $(this).parent().prev().html(inputConfig[inputName]["label"]+' =');
        $(this).attr('placeholder', inputConfig[inputName]["ph"]);
        $(this).attr('min', inputConfig[inputName]["min"]);
        $(this).attr('max', inputConfig[inputName]["max"]);
        $(this).attr('required','required');
        $(this).attr('autocomplete','off');
        $(this).attr('type','tel');
        $(this).attr('data-parsley-type','number');
        $(this).parent().next().html(inputConfig[inputName]["dimension"]);
    });

});