/**
 * AJAX код для отправки данных на
 * поверочный расчет и возврат результатов в блок results
 */
$(document).ready(function() {

    /**
     * Режим максимальной мощности
     */
    $('#form-method-forward-capacity').submit(function(e) {
        e.preventDefault();
        $.ajax({

            beforeSend: function () {
                //Начинается работа прелоадера
                $('.be-loading').addClass('be-loading-active');
            },

            complete: function () {
                //Заканчивается работа прелоадера
                $('.be-loading').removeClass('be-loading-active');
            },

            type: 'POST',
            url: '/transformer/methodForwardCapacity',
            data: $(this).serialize(),

        }).done(function (response) {
            const jsonData = JSON.parse(response);

            //Удаляем предыдущий вывод результатов
            clearPreviousResults();

            //Помещаем посчитаные размеры трансформатора
            // в блок результатов
            putForwardCapacityResults(jsonData);

        }).fail(function () {
            //Сообщение об ошибке
            alert('Что-то пошло не так!');
        });
    });

    /**
     * Режим максимального КПД
     */
    $('#form-method-forward-efficiency').submit(function(e) {
        e.preventDefault();
        $.ajax({

            beforeSend: function () {
                //Начинается работа прелоадера
                $('.be-loading').addClass('be-loading-active');
            },

            complete: function () {
                //Заканчивается работа прелоадера
                $('.be-loading').removeClass('be-loading-active');
            },

            type: 'POST',
            url: '/transformer/methodForwardEfficiency',
            data: $(this).serialize(),

        }).done(function (response) {
            const jsonData = JSON.parse(response);

            //Удаляем предыдущий вывод результатов
            clearPreviousResults();

            //Помещаем посчитаные размеры трансформатора
            // в блок результатов
            putForwardEfficiencyResults(jsonData);

        }).fail(function () {
            //Сообщение об ошибке
            alert('Что-то пошло не так!');
        });
    });

});

