/**
 * AJAX код для передачи id материалов
 * на удаление
 */
$(document).ready(function() {

    $('.btn-delete-submit').click(function(e) {
        const data = {
            'materialID' :getSelectedRowsID()
        };
        e.preventDefault();
        $.ajax({

            beforeSend: function () {
                //Начинается работа прелоадера
                $('.be-loading').addClass('be-loading-active');
                $('.close-delete').trigger('click');
            },

            complete: function () {
                //Заканчивается работа прелоадера
                $('.be-loading').removeClass('be-loading-active');
                table.ajax.reload();
            },

            type: 'POST',
            url: '/admin/deleteMaterial',
            data: data,

        }).done(function (response) {
            if (response === 'true') {
                buttonDelete.disable();
                buttonDelete.nodes().removeAttr('data-toggle');
                buttonDelete.nodes().removeAttr('data-target');
            } else {
                alert('Что-то пошло не так!');
            }
        }).fail(function () {
            //Сообщение об ошибке
            alert('Что-то пошло не так!');
        });
    });

});