/**
 * Общие функции страницы администратора
 */

/**
 * Получает id выделенных строк
 * и заносит в массив
 *
 * @returns array[string]
 */
function getSelectedRowsID() {
    const rows = $('table>tbody').find('.selected');
    let id = [];
    rows.each(function () {
        id.push($(this).attr('id').slice(4));
    });
    return id;
}
