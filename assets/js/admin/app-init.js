/**
 * Файл настроек подключаемых модулей
 */

$(document).ready(function(){

    /**
     * Инициализируем JS
     */
    App.init();

    /**
     * Валидация
     */
    $('form').parsley();

    /*Замена запятой на точку*/
    $("input[data-parsley-type='number']").on( "keyup", function() {
        this.value = this.value.replace(/,/g,".");
    });

    /**
     * Таблица
     */
    App.dataTables();
});