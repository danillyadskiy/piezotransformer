/**
 * Файл отвечает за настройки таблицы
 */
const table = $('#table').DataTable({
    ajax: {
        url: '/admin/table',
        method: 'POST',
    },
    "bAutoWidth":false,
    scrollX: true,
    language: {
        "processing": "Подождите...",
        "search": "Поиск:",
        "lengthMenu": "Показать _MENU_ записей",
        "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
        "infoEmpty": "Записи с 0 до 0 из 0 записей",
        "infoFiltered": "",
        "infoPostFix": "",
        "loadingRecords": "Загрузка записей...",
        "zeroRecords": "Записи отсутствуют",
        "emptyTable": "В таблице отсутствуют данные",
        "paginate": {
            "first": "Первая",
            "previous": "Предыдущая",
            "next": "Следующая",
            "last": "Последняя"
        },
        "aria": {
            "sortAscending": ": активировать для сортировки столбца по возрастанию",
            "sortDescending": ": активировать для сортировки столбца по убыванию"
        },
        select: {
            rows: {
                _: "Выбрано строк %d",
                0: ""
            }
        }
    },
    order: [[ 0, "desc" ]],
    select: true,
    columns: [
        {data: "id"},
        {data: "Material"},
        {data: "Company"},
        {data: "Country"},
        {data: "ro"},
        {data: "Kp"},
        {data: "Qm"},
        {data: "Qs11e"},
        {data: "Qs12e"},
        {data: "Qs13e"},
        {data: "Qs33e"},
        {data: "Qs44e"},
        {data: "Qd31"},
        {data: "Qd33"},
        {data: "Qd15"},
        {data: "Qe11t"},
        {data: "Qe33t"},
        {data: "Y11e"},
        {data: "Y33e"},
        {data: "Y33d"},
        {data: "c11e"},
        {data: "jc11e"},
        {data: "c12e"},
        {data: "jc12e"},
        {data: "c13e"},
        {data: "jc13e"},
        {data: "c33e"},
        {data: "jc33e"},
        {data: "c44e"},
        {data: "jc44e"},
        {data: "c11d"},
        {data: "jc11d"},
        {data: "c12d"},
        {data: "jc12d"},
        {data: "c13d"},
        {data: "jc13d"},
        {data: "c33d"},
        {data: "jc33d"},
        {data: "c44d"},
        {data: "jc44d"},
        {data: "s11e"},
        {data: "js11e"},
        {data: "s12e"},
        {data: "js12e"},
        {data: "s13e"},
        {data: "js13e"},
        {data: "s33e"},
        {data: "js33e"},
        {data: "s44e"},
        {data: "js44e"},
        {data: "s11d"},
        {data: "js11d"},
        {data: "s12d"},
        {data: "js12d"},
        {data: "s13d"},
        {data: "js13d"},
        {data: "s33d"},
        {data: "js33d"},
        {data: "s44d"},
        {data: "js44d"},
        {data: "e31"},
        {data: "je31"},
        {data: "e33"},
        {data: "je33"},
        {data: "e15"},
        {data: "je15"},
        {data: "d31"},
        {data: "jd31"},
        {data: "d33"},
        {data: "jd33"},
        {data: "d15"},
        {data: "jd15"},
        {data: "h31"},
        {data: "jh31"},
        {data: "h33"},
        {data: "jh33"},
        {data: "h15"},
        {data: "jh15"},
        {data: "g31"},
        {data: "jg31"},
        {data: "g33"},
        {data: "jg33"},
        {data: "g15"},
        {data: "jg15"},
        {data: "e11s"},
        {data: "je11s"},
        {data: "e11t"},
        {data: "je11t"},
        {data: "e33s"},
        {data: "je33s"},
        {data: "e33t"},
        {data: "je33t"},
    ],
    buttons: [
        {
            text: 'Удалить',
            className: 'button-delete',
            enabled: false
        },
        {
            extend: 'excel',
            text: 'Excel'
        }
    ],
    dom:  "<'row be-datatable-header'<'col-sm-2'l><'col-sm-6'B><'col-sm-4 text-right'f>>" +
        "<'row be-datatable-body'<'col-sm-12'tr>>" +
        "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
});

const buttonDelete = table.buttons( ['.button-delete'] );

table.on( 'select', function ( e, dt, type, indexes ) {
    buttonDelete.enable();
    buttonDelete.nodes().attr('data-toggle', 'modal');
    buttonDelete.nodes().attr('data-target', '#mod-primary');
});

table.on( 'deselect', function ( e, dt, type, indexes ) {
    if ( table.rows( { selected: true } ).indexes().length === 0 ) {
        buttonDelete.disable();
        buttonDelete.nodes().removeAttr('data-toggle');
        buttonDelete.nodes().removeAttr('data-target');
    }
});