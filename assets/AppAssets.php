<?php

/**
 * Описывает стили, требуемые представлением
 * и генерирует HTML код
 *
 * @var mixed $jsAssets
 * @var mixed $cssAssets
 */
class AppAssets
{
    private static $cssAssets = [
        'transformer' => [
            '/public/assets/lib/perfect-scrollbar/css/perfect-scrollbar.css',
            '/public/assets/lib/material-design-icons/css/material-design-iconic-font.min.css',
            '/public/assets/lib/select2/css/select2.min.css',
            '/public/assets/lib/bootstrap-slider/css/bootstrap-slider.min.css',
            '/public/assets/css/app.min.css',
            '/assets/css/restyle.css',
            '/assets/css/transformer.css',
        ],
        'admin' => [
            '/public/assets/lib/material-design-icons/css/material-design-iconic-font.min.css',
            '/public/assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
            '/public/assets/lib/datatables/datatables.net-select/css/select.bootstrap4.min.css',
            '/public/assets/css/app.min.css',
            '/assets/css/restyle.css',
            '/assets/css/admin.css',
        ],
        'error' => [
            '/public/assets/css/app.min.css',
        ],
    ];
    private static $jsAssets = [
        'transformer' => [
            '/public/assets/lib/jquery/jquery.min.js',
            '/public/assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js',
            '/public/assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js',
            '/public/assets/js/app.js',
            '/public/assets/lib/select2/js/select2.full.min.js',
            '/public/assets/lib/bootstrap-slider/bootstrap-slider.min.js',
            '/public/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js',
            '/public/assets/js/app-form-elements.js',
            '/public/assets/lib/parsley/parsley.min.js',
            '/public/assets/lib/parsley/i18n/ru.js',
            '/public/assets/lib/jquery-flot/jquery.flot.js',
            '/public/assets/lib/jquery-flot/jquery.flot.axislabels.js',
            '/public/assets/lib/jquery-flot/jquery.flot.resize.js',
            '/public/assets/lib/jquery-flot/plugins/jquery.flot.tooltip.js',
            '/assets/js/transformer/transformer.js',
            '/assets/js/transformer/flot.js',
            '/assets/js/transformer/input-config.js',
            '/assets/js/transformer/app-init.js',
            '/assets/js/transformer/ajax-method-forward.js',
            '/assets/js/transformer/ajax-method-backward.js',
        ],
        'admin' => [
            '/public/assets/lib/jquery/jquery.min.js',
            '/public/assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js',
            '/public/assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js',
            '/public/assets/js/app.js',
            '/public/assets/lib/parsley/parsley.min.js',
            '/public/assets/lib/parsley/i18n/ru.js',
            '/public/assets/lib/datatables/datatables.net/js/jquery.dataTables.min.js',
            '/public/assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
            '/public/assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js',
            '/public/assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js',
            '/public/assets/lib/datatables/jszip/jszip.min.js',
            '/public/assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js',
            '/public/assets/lib/datatables/datatables.net-select/js/dataTables.select.min.js',
            '/public/assets/js/app-tables-datatables.js',
            '/assets/js/admin/app-init.js',
            '/assets/js/admin/table.js',
            '/assets/js/admin/admin.js',
            '/assets/js/admin/ajax-delete-rows.js',
        ],
    ];

    /**
     * Генерация HTML кода для подключения css-файлов
     *
     * @param string $template
     * @return string
     */
    public static function getCss($template)
    {
        $link = '';
        foreach (self::$cssAssets as $key => $value) {
            if ($template == $key) {
                foreach ($value as $css) {
                    $link .= "<link rel=\"stylesheet\" href=\"$css\" type=\"text/css\"/>";
                }
                break;
            }
        }
        return $link;
    }

    /**
     * Генерация HTML кода для подключения js-файлов
     *
     * @param string $template
     * @return string
     */
    public static function getJs($template)
    {
        $script = '';
        foreach (self::$jsAssets as $key => $value) {
            if ($template == $key) {
                foreach ($value as $js) {
                    $script .= "<script src=\"$js\" type=\"text/javascript\"></script>";
                }
                break;
            }
        }
        return $script;
    }
}

//EOF